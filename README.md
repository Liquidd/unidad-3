Adapter

Un adaptador envuelve uno de los objetos para ocultar la complejidad de la conversión que ocurre detrás de escena. El objeto envuelto ni siquiera conoce el adaptador. 
Por ejemplo, puede envolver un objeto que opera en metros y kilómetros con un adaptador que convierte todos los datos en unidades imperiales como pies y millas.

Factory Method

El patrón Método de fábrica sugiere que reemplace las llamadas de construcción de objetos directos (utilizando el operador new ) con llamadas a un método de fábrica especial .
los objetos todavía se crean a través del operador new , pero se llama desde el método de fábrica. Los objetos devueltos por un método de fábrica a menudo se denominan "productos".

Prototype

El patrón Prototype delega el proceso de clonación a los objetos reales que se están clonando. El patrón declara una interfaz común para todos los objetos que admiten la clonación. 
Esta interfaz le permite clonar un objeto sin acoplar su código a la clase de ese objeto. Por lo general, dicha interfaz contiene un solo clonemétodo

Singleton

 Puede estar seguro de que una clase tiene una sola instancia.
 Obtiene un punto de acceso global a esa instancia.
 El objeto singleton se inicializa solo cuando se solicita por primera vez.
 
 Proxy
 
  Si necesita ejecutar algo antes o después de la lógica primaria de la clase, el proxy le permite hacer esto sin cambiar esa clase. 
  Como el proxy implementa la misma interfaz que la clase original, se puede pasar a cualquier cliente que espere un objeto de servicio real.
  
 Patron de Diseño Seleccionado : Protoype
 
 En algunos casos, el coste de crear un objeto nuevo desde 0 es muy elevado, y más aún si luego hay que establecer una gran colección de atributos. En éste contexto sería más conveniente clonar un objeto predeterminado que actúe de prototipo y modificar los valores necesarios para que se ajuste a su nuevo propósito.
 Se aplica cuando:
    La creación de nuevos objetos acarrea un coste computacional elevado..
    Los objetos a crear tienen o suelen tener atributos que repiten su valor.
    
Inyeccion de Dependencias Aplicado en en mi proyecto
en mi proyecto la inyeccion de dependencias se aplica para los mensajes personalizados de cada nivel de usuario de manera que si deseo cambiar el mensaje personalizado para un usuario en especifico unicamente debo crear una clase que extienda de la clase rol y pasarla como dependencia de la clase usuario
de esta manera hago explicita la delacracion de la clase y queda descoplado la clase usuario de la clase Rol
 
 
  fuentes
  
    https://refactoring.guru
    https://sourcemaking.com
    https://alanchavez.com/como-usar-inyeccion-de-dependencias-en-php/
    https://programacion.net/articulo/patrones_de_diseno_v_patrones_de_creacion_prototipo_1005
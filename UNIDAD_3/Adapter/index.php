<?php
class Post
{
    private $author;
    private $title;
    private $body;

    public function __construct($author = "", $title = "", $body ="")
    {
        $this->author = $author;
        $this->title = $title;
        $this->body = $body;
    }

    public function author()
    {
        return $this->author;
    }

    public function title()
    {
        return $this->title;
    }

    public function body ()
    {
        return $this->body;
    }
}
class PostAdapter
{
    private $post = null;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function getPostInfo()
    {
        return "{$this->post->title()}, author: {$this->post->author()}";
    }

    public function description ()
    {
        return $this->post->body();
    }
}

$post = new PostAdapter(new Post("Steve", "Breaking News", "Lorem ipsum dolo..."));

echo $post->getPostInfo(); // Devuelve: Breaking News, author: Steve
echo $post->description(); // Devuelve: Lorem ipsum dolo...
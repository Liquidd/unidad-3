<?php

class Rol {
	public function __construct(){}
	public function bienvenido($mensaje) {
		echo "Bienvenido: {$mensaje}";
	}
}
class Administrador extends Rol {
	public function __construct() {}
}
class Operador extends Rol {
	public function __construct() {}
}
class Usuario {
	private $Rol;
	public function __construct(Rol $Rol,$mensaje) {
		$this->Rol = $Rol;
		$this->Rol->bienvenido($mensaje);
	}
}
$user_operador = new Operador();
$user_administrador = new Administrador();



$bienvenida_operaciones = new Usuario($user_operador,"usuario de operaciones");
$bienvenido_administrador = new Usuario($user_administrador ,"Administrador");

$bienvenida_operaciones->tipo->bienvenido();
echo "<br>";
$bienvenido_administrador->tipo->bienvenido();

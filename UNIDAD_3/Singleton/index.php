<?php

class DataBase {
    static private $instance = null;
     
    private function __contruct() {
    }
     
    public static function getInst() {
        if (self::$instance == null) {
            self::$instance = new DataBase();
        }
        return self::$instance;
    }

    public function connectToDB($data) {
        //Codigo
    }
         
    public function selectQuery($data) {
        //Codigo
    }
         
    public function insertQuery($data) {
        //Codigo
    }
}

$db = Database::getInst();
$db->connectToDB($data);
 
//Exactamente lo mismo, pero sin utilizar variables
Database::getInst()->connectToDB($data);


$db = new DataBase();
//Nos retornara un error
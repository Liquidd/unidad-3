<?php 
class Foo { 
    public $aMemberVar = 'aMemberVar Member Variable'; 
    public $aFuncName = 'aMemberFunc'; 
    
    
    function aMemberFunc() { 
        print 'Inside `aMemberFunc()`'; 
    } 
} 

$foo = new Foo;

$variable =  $foo->aMemberVar;
echo "nombre de la variable ".$variable;
$foo->aMemberFunc();

?> 
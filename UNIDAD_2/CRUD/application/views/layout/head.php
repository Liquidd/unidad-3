<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?= $titulo ?></title>
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="max-age=604800" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="Bootstrap-ecommerce by Vosidiy">



<!-- cambios personales -->
<link href="<?php echo base_url()?>bootstrap_UI/images/favicon.ico" rel="shortcut icon" type="image/x-icon">
<link href="<?php echo base_url()?>assets/css/personal.css" rel="stylesheet" type="text/css"/>

<!-- jQuery -->
<script src="<?php echo base_url()?>node_modules/jquery/dist/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>node_modules/jquery/dist/jquery.js" type="text/javascript"></script>


<!-- jQuery Personal -->
<script src="<?php echo base_url()?>assets/js/app.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/productos.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/personal.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>node_modules/sweetalert/dist/sweetalert.min.js" type="text/javascript"></script>

<!-- Bootstrap4 files-->
<script src="<?php echo base_url()?>bootstrap_UI/js/bootstrap.bundle.min.js" type="text/javascript"></script>
<link href="<?php echo base_url()?>bootstrap_UI/css/bootstrap-custom.css" rel="stylesheet" type="text/css"/>

<!-- Font awesome 5 -->
<link href="<?php echo base_url()?>bootstrap_UI/fonts/fontawesome/css/fontawesome-all.min.css" type="text/css" rel="stylesheet">

<!-- custom style -->
<link href="<?php echo base_url()?>bootstrap_UI/css/uikit.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url()?>bootstrap_UI/css/responsive.css" rel="stylesheet" media="only screen and (max-width: 1200px)" />

<!-- custom javascript -->
<script src="<?php echo base_url()?>bootstrap_UI/js/script.js" type="text/javascript"></script>

<!-- plugin: fancybox  -->
<script src="<?php echo base_url()?>bootstrap_UI/plugins/fancybox/fancybox.min.js" type="text/javascript"></script>
<link href="<?php echo base_url()?>bootstrap_UI/plugins/fancybox/fancybox.min.css" type="text/css" rel="stylesheet">

<!-- plugin: owl carousel  -->
<link href="<?php echo base_url()?>bootstrap_UI/plugins/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>bootstrap_UI/plugins/owlcarousel/assets/owl.theme.default.css" rel="stylesheet">
<script src="<?php echo base_url()?>bootstrap_UI/plugins/owlcarousel/owl.carousel.min.js"></script>

<!-- plugin: slickslider -->
<link href="<?php echo base_url()?>bootstrap_UI/plugins/slickslider/slick.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>bootstrap_UI/plugins/slickslider/slick-theme.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url()?>bootstrap_UI/plugins/slickslider/slick.min.js"></script>
<script>
    var base_url = "<?php echo base_url();?>";
    var cont = 0;
</script>
<style>
    @import url('https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900');
    .detalles:hover{
        font-weight: bold;
    }
    body{
        font-family: 'Roboto', sans-serif;
    }

    .content-page{
        margin-top: 50px;
        margin-bottom: 150px;
        border-top: 1px solid #646363;
        border-bottom: 1px solid #646363;
    }
    .footer-principal{
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    }
    .pago_fila{
        margin-bottom: 250px;
        padding-bottom: 250px;
    }
    .btn-light{
        border-color:   black;
        font-weight: bold;
    }
    .blog .carousel-indicators {
        left: 0;
        top: auto;
        bottom: -40px;

    }

    /* The colour of the indicators */
    .blog .carousel-indicators li {
        background: #a3a3a3;
        border-radius: 50%;
        width: 8px;
        height: 8px;
    }

    .blog .carousel-indicators .active {
        background: #707070;
    }
</style>
</head>
<body>
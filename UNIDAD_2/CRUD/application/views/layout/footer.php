<footer class="section-footer bg-dark">
	<div class="container">
		<section class="footer-top padding-top">
			<div class="row">
				<aside class="col-sm-3 col-md-3 white">
					<h5 class="title">Direccion</h5>
					<p>
						Quintero Arce #32 | Col.San Andrés | Hermosillo Sonora Mexico
					</p>
				</aside>
				<aside class="col-sm-3  col-md-3 white">
					<h5 class="title">Perfil</h5>
					<ul class="list-unstyled">
						<li> <a href="#"> Mi Perfil </a></li>
						<li> <a href="#"> Registrarse </a></li>
						<li> <a href="#"> Configuracion de Cuenta </a></li>
						<li> <a href="#"> Mi Carrito </a></li>
						<li> <a href="#"> Lista de Deseos </a></li>
					</ul>
				</aside>
				<aside class="col-sm-3  col-md-3 white">
					<h5 class="title">Nosotros</h5>
					<ul class="list-unstyled">
						<li> <a href="#"> Acerca de Nosotros </a></li>
						<li> <a href="#"> Como Comprar </a></li>
					</ul>
				</aside>
				<aside class="col-sm-3">
					<article class="white">
						<h5 class="title">Contacto</h5>
						<p>
							Telefono: +123456789 <br> 
						</p>
						<p>
							www.gahipotecario.com
						</p>
						 <div class="btn-group white">
						    <a class="btn btn-facebook" title="Facebook" target="_blank" href="#"><i class="fab fa-facebook-f  fa-fw"></i></a>
						    <a class="btn btn-instagram" title="Instagram" target="_blank" href="#"><i class="fab fa-instagram  fa-fw"></i></a>
						    <a class="btn btn-youtube" title="Youtube" target="_blank" href="#"><i class="fab fa-youtube  fa-fw"></i></a>
						    <a class="btn btn-twitter" title="Twitter" target="_blank" href="#"><i class="fab fa-twitter  fa-fw"></i></a>
						</div>
					</article>
				</aside>
			</div> <!-- row.// -->
			<br> 
		</section>
		<section class="footer-bottom row border-top-white">
			<div class="col-sm-12">
				<p class="text-md-center text-white-50">
					Copyright © 2018 GA Recompensas
				</p>
			</div>
		</section> <!-- //footer-top -->
	</div><!-- //container -->
</footer>
